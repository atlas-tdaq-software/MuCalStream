#ifndef _MuCalStreamerSelector_h_
#define _MuCalStreamerSelector_h_

namespace SIOM
{
  class MuCalStreamerSelector
  {
   public:
    MuCalStreamerSelector (float emin, float emax, float pmin, float pmax)
      : _eta_min (emin), _eta_max (emax), _phi_min (pmin), _phi_max (pmax)
      {};

    virtual ~MuCalStreamerSelector ();

    float eta_min () {return _eta_min;};
    float eta_max () {return _eta_max;};
    float phi_min () {return _phi_min;};
    float phi_max () {return _phi_max;};

   private:
    float _eta_min;
    float _eta_max;
    float _phi_min;
    float _phi_max;
  };
}

#endif
