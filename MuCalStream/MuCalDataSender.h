#ifndef _MuCalDataSender_h_
#define _MuCalDataSender_h_

#include <unistd.h>
#include <errno.h>

#include <vector>
#include <string>
#include <mutex>

// Temporary copied from EventStorage

#include <MuCalStream/Guid.h> 

// TDAQ include files

#include "is/infodictionary.h"

// siom include files

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

// Package include files

#include "mucal_info/muCalServerOutput.h"

#include "MuCalStream/MuCalOutParameters.h"
#include "MuCalStream/HandShaker.h"

namespace SIOM
{
  class MuCalDataSender : public SIOM::DataSender
  {
   public:
    MuCalDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		     SIOM::DataDestination *dest);

    virtual ~MuCalDataSender ();

   private:
    virtual bool can_send ();
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b);

    virtual void startOfRun ();
    virtual void endOfRun ();

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
    std::string build_filename (std::string &location,
				std::string &file_tag,
				std::string &stream_type,
				std::string &stream_name,
				unsigned int run_number,
				unsigned int file_index,
				std::string &local_file_name);

    std::string build_datasetname (std::string &file_tag,
				   std::string &stream_type,
				   std::string &stream_name,
				   unsigned int run_number);

    std::string publish_file (std::string &filename,
			      std::string &local_filename);

   private:
    SIOM::MuCalOutParameters *_outpar;

    bool _firstInDataSet;
    bool _writeData;

    unsigned int _index;
    unsigned int _RunNumber;
    unsigned int _written;
    unsigned int _event_number;

    std::string _hostname;
    std::string _filename;
    std::string _local_filename;
    std::string _dataset_name;
    std::string _application_name;
    std::string _guid;

    HandShaker *_handshaker;

    uint32_t _checksum;
    int _descriptor;

    std::mutex _fileMutex;
  };
}

inline bool SIOM::MuCalDataSender::can_send ()
{
  return (true);
}

inline siom_info::siomPlugin *SIOM::MuCalDataSender::createInfo ()
{
  mucal_info::muCalServerOutput *info = new mucal_info::muCalServerOutput ();
  return info;
}

inline void SIOM::MuCalDataSender::fillSpecificInfo (ISInfo *info)
{
}

#endif
