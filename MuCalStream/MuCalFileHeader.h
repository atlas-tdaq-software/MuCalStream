#ifndef _MuCalFileHeader_h_
#define _MuCalFileHeader_h_

#include <vector>

#include "MuCalStream/MuCalStreamerSelector.h"

namespace SIOM
{
  class MuCalFileHeader
  {
   public:
    enum CompressorType
    {
      none = 0,
      zlib,
      bzlib
    };

    MuCalFileHeader (unsigned int compressor_type,
		     unsigned int compression_level,
		     std::vector <SIOM::MuCalStreamerSelector> &selectors);


    virtual ~MuCalFileHeader ();

    // Data access

    unsigned int   header_length ();
    unsigned char *header_buffer ();

   private:
    static const uint32_t MuCalFileHeaderMagic   = 0x11223344;
    static const uint32_t MuCalFileFormatVersion = 0x0100;

    unsigned int _compressor_type;
    unsigned int _compression_level;

    struct
    {
      float emin;
      float emax;
      float pmin;
      float pmax;
    } _selector_structure;

    unsigned int   _header_length;
    unsigned char *_header_buffer;
  };
}

inline SIOM::MuCalFileHeader::~MuCalFileHeader ()
{
  delete _header_buffer;
}

inline unsigned int SIOM::MuCalFileHeader::header_length ()
{
  return _header_length;
}

inline unsigned char *SIOM::MuCalFileHeader::header_buffer ()
{
  return _header_buffer;
}

#endif
