#ifndef _MuCalStreamer_h_
#define _MuCalStreamer_h_

#include <vector>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMProcessor.h"
#include "siom/core/SIOMDataProcessor.h"

#include "MuCalStream/MuCalStreamerDataProcessor.h"
#include "MuCalStream/MuCalStreamerSelector.h"

namespace SIOM
{
  class MuCalStreamer : public SIOM::SIOMProcessor
  {
   public:
    MuCalStreamer () {};
    virtual ~MuCalStreamer () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataProcessor *createProcessor ();

   private:
    SIOM::MuCalStreamerDataProcessor *_processor;
    std::vector <SIOM::MuCalStreamerSelector> _selectors;
  };
}

inline SIOM::MuCalStreamer::~MuCalStreamer () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataProcessor *SIOM::MuCalStreamer::createProcessor ()
{
  _processor = new SIOM::MuCalStreamerDataProcessor (this->input_memory_pools (),
						     this->output_memory_pools (),
						     0);
  return _processor;
}

#endif
