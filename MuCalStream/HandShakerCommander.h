#ifndef _HandShakerCommander_h_
#define _HandShakerCommander_h_

// STL include files

#include <string>
#include <map>
#include <queue>
#include <mutex>

// SFOTZ include files

#include "SFOTZ/DBConnection.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/IndexTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/SFOTZIssues.h"

// SIOM include files

#include "MuCalStream/DBParameters.h"

namespace SIOM
{
  class HandShakerCommander
  {
   public:
    typedef enum {RUNOPENED, RUNCLOSED,
		  LBOPENED, LBCLOSED,
		  FILEOPENED, FILECLOSED} CommandType;

    HandShakerCommander (std::string &dbAddress,
			 std::string &indextable,
			 std::string &runtable,
			 std::string &filetable,
			 std::string &lumitable);

    virtual ~HandShakerCommander ();

    // Methods to queue commands

    void file_open  (std::string &local_file_name,
		     unsigned int file_number,
		     std::string &application_name,
		     std::string &host_name,
		     unsigned int run_number,
		     unsigned int luminosity_block,
                     std::string &project_tag,
		     std::string &stream_type,
		     std::string &stream,
		     std::string &data_stream_name,
		     std::string &guid,
		     unsigned int file_size,
		     std::string &checksum,
		     unsigned int event_number,
		     std::string &complete_file_name,
		     bool isfirst);

    void file_close (std::string &local_file_name,
		     unsigned int file_number,
		     std::string &application_name,
		     std::string &host_name,
		     unsigned int run_number,
		     unsigned int luminosity_block,
                     std::string &project_tag,
		     std::string &stream_type,
		     std::string &stream,
		     std::string &data_stream_name,
		     std::string &guid,
		     unsigned int file_size,
		     std::string &checksum,
		     unsigned int event_number,
		     std::string &complete_file_name,
		     bool islast);

    // Methods to check and execute commands

    bool test_command ();
    void execute_command ();

   private:
    void open_connection ();
    void close_connection ();
    void connect_tables ();
    void disconnect_tables ();

    void queue_command (SIOM::HandShakerCommander::CommandType, SIOM::DBParameters);

    void add_run  (SIOM::RunParameters *r);
    void add_file (SIOM::FileParameters *f);
    void add_lumi (SIOM::LumiBlockParameters *l);

    void close_run  (SIOM::RunParameters *r);
    void close_file (SIOM::FileParameters *f);
    void close_lumi (SIOM::LumiBlockParameters *l);

   private:
    std::string _dbAddress;

    std::string _indexTableName;
    std::string _runTableName;
    std::string _fileTableName;
    std::string _lumiTableName;

    SFOTZ::DBConnection   *_db;
    SFOTZ::IndexTable     *_index_table;
    SFOTZ::RunTable       *_run_table;
    SFOTZ::FileTable      *_file_table;
    SFOTZ::LumiBlockTable *_lumi_table;

    std::mutex _q_mutex;
    std::queue <std::pair <SIOM::HandShakerCommander::CommandType, SIOM::DBParameters> > _command_queue;
  };
}

inline void SIOM::HandShakerCommander::open_connection ()
{
  _db = new SFOTZ::DBConnection (_dbAddress);
}

inline void SIOM::HandShakerCommander::close_connection ()
{
  if (_db)
    delete _db;
}

inline void SIOM::HandShakerCommander::connect_tables ()
{
  _index_table = new SFOTZ::IndexTable     (*_db, _indexTableName);
  _run_table   = new SFOTZ::RunTable       (*_db, _runTableName);
  _file_table  = new SFOTZ::FileTable      (*_db, _fileTableName);
  _lumi_table  = new SFOTZ::LumiBlockTable (*_db, _lumiTableName);
}

inline void SIOM::HandShakerCommander::disconnect_tables ()
{
  if (_index_table)
    delete _index_table;

  if (_run_table)
    delete _run_table;

  if (_file_table)
    delete _file_table;

  if (_lumi_table)
    delete _lumi_table;
}

inline void SIOM::HandShakerCommander::add_run (SIOM::RunParameters *r)
{
  _run_table->addOpenedRunEntry (r->application_name (),
				 r->run_number (),
				 r->stream_type (),
				 r->stream (),
				 r->dsname (),
				 r->project (),
                                 1,
				 _index_table,
				 false);
}

inline void SIOM::HandShakerCommander::add_file (SIOM::FileParameters *f)
{
  _file_table->addOpenedFileEntry (f->local_file_name (),
                                   f->file_number (),
				   f->application_name (),
				   f->host_name (),
				   f->run_number (),
				   f->luminosity_block (),
                                   f->stream_type (),
                                   f->stream (),
                                   f->guid (),
				   f->complete_file_name (),
				   _index_table,
				   false);
}

inline void SIOM::HandShakerCommander::add_lumi (SIOM::LumiBlockParameters *l)
{
  _lumi_table->addOpenedLumiBlockEntry (l->application_name (),
                                        l->run_number (),
					l->luminosity_block (),
					l->stream_type (),
                                        l->stream (),
                                        1,
					_index_table,
                                        false);
}

inline void SIOM::HandShakerCommander::close_run (SIOM::RunParameters *r)
{
  _run_table->setRunEntryClosed (r->application_name (),
				 r->run_number (),
				 r->stream_type (),
				 r->stream (),
				 false);
}

inline void SIOM::HandShakerCommander::close_file (SIOM::FileParameters *f)
{
  _file_table->setFileEntryClosed (f->local_file_name (),
				   f->file_number (),
				   f->application_name (),
				   f->run_number (),
				   f->luminosity_block (),
				   f->stream_type (),
				   f->stream (),
				   f->file_size (),
				   f->checksum (),
				   f->event_number (),
				   f->complete_file_name (),
				   false);
}

inline void SIOM::HandShakerCommander::close_lumi (SIOM::LumiBlockParameters *l)
{
  _lumi_table->setLumiBlockEntryClosed (l->application_name (),
                                        l->run_number (),
					l->luminosity_block (),
					l->stream_type (),
                                        l->stream (),
                                        false);
}

inline bool SIOM::HandShakerCommander::test_command ()
{
  return (! _command_queue.empty ());
}

inline void SIOM::HandShakerCommander::queue_command (SIOM::HandShakerCommander::CommandType command_name,
						      SIOM::DBParameters command_parameters)
{
  std::pair <SIOM::HandShakerCommander::CommandType, SIOM::DBParameters> q_elem;

  q_elem.first = command_name;
  q_elem.second = command_parameters;

  std::lock_guard <std::mutex> lock (_q_mutex);
  _command_queue.push (q_elem);
}

#endif
