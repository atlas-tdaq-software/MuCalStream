#ifndef _MuCalOutParameters_h_
#define _MuCalOutParameters_h_

#include <string>
#include "ipc/partition.h"

#include "siom/core/SIOMCoreConfig.h"

namespace SIOM
{
  class MuCalOutParameters
  {
   public:
    MuCalOutParameters (std::string &activation_flag,
			IPCPartition *ipc_partition,
			std::string &file_location,
			std::string &file_tag,
			std::string &stream_type,
			std::string &stream_name,
			unsigned int maxsize,
			std::string &compressor_type,
			unsigned int compression_level,
			SIOM::SIOMCoreConfig *core_conf)
      : _activationflag (activation_flag),
        _ipcpartition (ipc_partition),
        _filelocation (file_location), _filetag (file_tag),
        _streamtype (stream_type), _streamname (stream_name),
        _maxsize (maxsize), _compressor_type (compressor_type),
        _compression_level (compression_level), _core_conf (core_conf)
      {};

    ~MuCalOutParameters () {};

    // Data access

    std::string &activation_flag ();
    IPCPartition *ipc_partition ();
    std::string &file_location ();
    std::string &file_tag ();
    std::string &stream_type ();
    std::string &stream_name ();
    unsigned int maxsize ();
    std::string &compressor_type ();
    unsigned int compression_level ();
    SIOM::SIOMCoreConfig *core_conf ();

   private:
    std::string _activationflag;
    IPCPartition *_ipcpartition;
    std::string _filelocation;
    std::string _filetag;
    std::string _streamtype;
    std::string _streamname;
    unsigned int _maxsize;
    std::string _compressor_type;
    unsigned int _compression_level;
    SIOM::SIOMCoreConfig *_core_conf;
  };
}

inline std::string &SIOM::MuCalOutParameters::activation_flag ()
{
  return _activationflag;
}

inline IPCPartition *SIOM::MuCalOutParameters::ipc_partition ()
{
  return _ipcpartition;
}

inline std::string &SIOM::MuCalOutParameters::file_location ()
{
  return _filelocation;
}

inline std::string &SIOM::MuCalOutParameters::file_tag ()
{
  return _filetag;
}

inline std::string &SIOM::MuCalOutParameters::stream_type ()
{
  return _streamtype;
}

inline std::string &SIOM::MuCalOutParameters::stream_name ()
{
  return _streamname;
}

inline unsigned int SIOM::MuCalOutParameters::maxsize ()
{
  return _maxsize;
}

inline std::string &SIOM::MuCalOutParameters::compressor_type ()
{
  return _compressor_type;
}

inline unsigned int SIOM::MuCalOutParameters::compression_level ()
{
  return _compression_level;
}

inline SIOM::SIOMCoreConfig *SIOM::MuCalOutParameters::core_conf ()
{
  return _core_conf;
}

#endif
