#ifndef _MuCalStreamerDataProcessor_h_
#define _MuCalStreamerDataProcessor_h_

#include <map>
#include <string>
#include <vector>

// TDAQ include files

#include "ROSBufferManagement/Buffer.h"

// Package include files

#include "mucal_info/muCalStreamer.h"

#include "siom/core/InternalPool.h"
#include "siom/core/AlgoParams.h"
#include "siom/core/DataProcessor.h"

namespace SIOM
{
  class MuCalStreamerDataProcessor : public SIOM::DataProcessor
  {
   public:
    MuCalStreamerDataProcessor (std::vector <std::pair <std::string, InternalPool *> > &inputPoolVector,
				std::vector <std::pair <std::string, InternalPool *> > &outputPoolVector) :
      SIOM::DataProcessor (inputPoolVector, outputPoolVector) 
    {
      this->start ();
    };

    virtual ~MuCalStreamerDataProcessor ()
    {
      this->join ()
    };

   private:
    virtual unsigned int process_data (ROS::Buffer *in, ROS::Buffer *out);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
  };
}

inline siom_info::siomPlugin *SIOM::MuCalStreamerDataProcessor::createInfo ()
{
  mucal_info::muCalStreamer *info = new mucal_info::muCalStreamer ();
  return info;
}

inline void SIOM::MuCalStreamerDataProcessor::fillSpecificInfo (ISInfo *info)
{
}

#endif
