#ifndef _MuCalServerOutput_h_
#define _MuCalServerOutput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMOutput.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "MuCalStream/MuCalOutParameters.h"
#include "MuCalStream/MuCalDataSender.h"

namespace SIOM
{
  class MuCalServerOutput : public SIOM::SIOMOutput
  {
   public:
    MuCalServerOutput () : _addr (0) {};
    virtual ~MuCalServerOutput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataSender *createSender ();

   private:
    SIOM::DataDestination _dest;
    SIOM::MuCalOutParameters *_addr;
    SIOM::MuCalDataSender *_fileSender;
  };
}

inline SIOM::MuCalServerOutput::~MuCalServerOutput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataSender *SIOM::MuCalServerOutput::createSender ()
{
  _fileSender = new SIOM::MuCalDataSender (this->memory_pools (),
					   &_dest);

  return _fileSender;
}

#endif
