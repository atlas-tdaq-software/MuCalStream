#ifndef _HandShaker_h_
#define _HandShaker_h_

#include <string>

// TDAQ include files

#include <queue>

// SFOTZ include files

#include "SFOTZ/DBConnection.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/IndexTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/SFOTZIssues.h"

// SIOM include files

#include "siom/core/SIOMActiveObject.h"

#include "MuCalStream/HandShakerCommander.h"

// Since the calls to CORAL DB interface are not thread safe,
// this class must be used as a singleton.

namespace SIOM
{
  class HandShaker : public SIOM::ActiveObject
  {
   public:
    HandShaker (std::string &dbAddress,
		std::string &indextable,
		std::string &runtable,
		std::string &filetable,
		std::string &lumitable);

    virtual ~HandShaker ();

   private:
    virtual void run ();

    void execute_all ();

   public:
    // Commands to the handshaker thread

    void file_open (std::string &local_file_name,
		    unsigned int file_number,
		    std::string &application_name,
		    std::string &host_name,
		    unsigned int run_number,
		    unsigned int luminosity_block,
                    std::string &project_tag,
		    std::string &stream_type,
		    std::string &stream,
		    std::string &data_stream_name,
		    std::string &guid,
		    unsigned int file_size,
		    std::string &checksum,
		    unsigned int event_number,
		    std::string &complete_file_name,
		    bool isfirst);

    void file_close (std::string &local_file_name,
		     unsigned int file_number,
		     std::string &application_name,
		     std::string &host_name,
		     unsigned int run_number,
		     unsigned int luminosity_block,
                     std::string &project_tag,
		     std::string &stream_type,
		     std::string &stream,
		     std::string &data_stream_name,
		     std::string &guid,
		     unsigned int file_size,
		     std::string &checksum,
		     unsigned int event_number,
		     std::string &complete_file_name,
		     bool islast);

   private:
    std::string _dbAddress;
    std::string _indexTableName;
    std::string _runTableName;
    std::string _fileTableName;
    std::string _lumiTableName;

    SIOM::HandShakerCommander *_hcommander;
  };
}

inline void SIOM::HandShaker::file_open (std::string &local_file_name,
					 unsigned int file_number,
					 std::string &application_name,
					 std::string &host_name,
					 unsigned int run_number,
					 unsigned int luminosity_block,
                                         std::string &project_tag,
					 std::string &stream_type,
					 std::string &stream,
					 std::string &data_stream_name,
					 std::string &guid,
					 unsigned int file_size,
					 std::string &checksum,
					 unsigned int event_number,
					 std::string &complete_file_name,
					 bool isfirst)
{
  _hcommander->file_open (local_file_name, file_number, application_name,
			  host_name, run_number, luminosity_block,
			  project_tag,
			  stream_type, stream, data_stream_name,
			  guid, file_size, checksum,
			  event_number, complete_file_name, isfirst);
}

inline void SIOM::HandShaker::file_close (std::string &local_file_name,
					  unsigned int file_number,
					  std::string &application_name,
					  std::string &host_name,
					  unsigned int run_number,
					  unsigned int luminosity_block,
                                          std::string &project_tag,
					  std::string &stream_type,
					  std::string &stream,
					  std::string &data_stream_name,
					  std::string &guid,
					  unsigned int file_size,
					  std::string &checksum,
					  unsigned int event_number,
					  std::string &complete_file_name,
					  bool islast)
{
  _hcommander->file_close (local_file_name, file_number, application_name,
			   host_name, run_number, luminosity_block,
			   project_tag,
			   stream_type, stream, data_stream_name,
			   guid, file_size, checksum,
			   event_number, complete_file_name, islast);
}

#endif
