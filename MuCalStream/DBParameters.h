#ifndef _DBParameters_h_
#define _DBParameters_h_

#include <string>

namespace SIOM
{
  class FileParameters
  {
   public:
    FileParameters (std::string &local_file_name,
		    unsigned int file_number,
		    std::string &application_name,
		    std::string &host_name,
		    unsigned int run_number,
		    unsigned int luminosity_block,
		    std::string &stream_type,
		    std::string &stream,
		    std::string &guid,
		    unsigned int file_size,
		    std::string &checksum,
		    unsigned int event_number,
		    std::string &complete_file_name) :
      _local_file_name (local_file_name),
      _file_number (file_number),
      _application_name (application_name),
      _host_name (host_name),
      _run_number (run_number),
      _luminosity_block (luminosity_block),
      _stream_type (stream_type),
      _stream (stream),
      _guid (guid),
      _file_size (file_size),
      _checksum (checksum),
      _event_number (event_number),
      _complete_file_name (complete_file_name)
      {};

    // Data access

    std::string &local_file_name ()
      {return _local_file_name;};

    unsigned int file_number ()
      {return _file_number;};

    std::string &application_name ()
      {return _application_name;};

    std::string &host_name ()
      {return _host_name;};

    unsigned int run_number ()
      {return _run_number;};

    unsigned int luminosity_block ()
      {return _luminosity_block;};

    std::string &stream_type ()
      {return _stream_type;};

    std::string &stream ()
      {return _stream;};

    std::string &guid ()
      {return _guid;};

    unsigned int file_size ()
      {return _file_size;};

    std::string &checksum ()
      {return _checksum;};

    unsigned int event_number ()
      {return _event_number;};

    std::string &complete_file_name ()
      {return _complete_file_name;}

   private:
    std::string _local_file_name;
    unsigned int _file_number;
    std::string _application_name;
    std::string _host_name;
    unsigned int _run_number;
    unsigned int _luminosity_block;
    std::string _stream_type;
    std::string _stream;
    std::string _guid;
    unsigned int _file_size;
    std::string _checksum;
    unsigned int _event_number;
    std::string _complete_file_name;
  };

  class RunParameters
  {
   public:
    RunParameters (std::string &application_name,
		   unsigned int run_number,
		   std::string &stream_type,
		   std::string &stream,
		   std::string &dsname,
		   std::string &project) :
      _application_name (application_name),
      _run_number (run_number),
      _stream_type (stream_type),
      _stream (stream),
      _dsname (dsname),
      _project (project)
      {};

    // Data access

    std::string &application_name ()
      {return _application_name;};

    unsigned int run_number ()
      {return _run_number;};

    std::string &stream_type ()
      {return _stream_type;};

    std::string &stream ()
      {return _stream;};

    std::string &dsname ()
      {return _dsname;};

    std::string &project ()
      {return _project;};

   private:
    std::string _application_name;
    unsigned int _run_number;
    std::string _stream_type;
    std::string _stream;
    std::string _dsname;
    std::string _project;
  };

  class LumiBlockParameters
  {
   public:
    LumiBlockParameters (std::string &application_name,
			 unsigned int run_number,
			 unsigned int luminosity_block,
			 std::string &stream_type,
			 std::string &stream) :
      _application_name (application_name),
      _run_number (run_number),
      _luminosity_block (luminosity_block),
      _stream_type (stream_type),
      _stream (stream)
      {};

    // Data access

    std::string &application_name ()
      {return _application_name;};

    unsigned int run_number ()
      {return _run_number;};

    unsigned int luminosity_block ()
      {return _luminosity_block;};

    std::string &stream_type ()
      {return _stream_type;};

    std::string &stream ()
      {return _stream;};

   private:
    std::string _application_name;
    unsigned int _run_number;
    unsigned int _luminosity_block;
    std::string _stream_type;
    std::string _stream;
  };

  union DBParPointer
  {
    SIOM::FileParameters *file_parameters;
    SIOM::RunParameters *run_parameters;
    SIOM::LumiBlockParameters *lumi_parameters;
  };

  typedef union DBParPointer DBParameters;
}

#endif
