import os
import re
import sys
import shutil
from xml.dom import minidom

def find_file (arg, d, flist):
    for f in flist:
        if f in superset:
            filename = os.path.join (d, f)
            filelist.append (filename)

def fix_file (arg, d, flist):
    for f in flist:
        infile  = os.path.join (d, f)
        outfile = '/tmp/' + f + '_tmp'
        fdin = open (infile, 'r')
        fdout = open (outfile, 'w')

        for line in fdin:
            if (re.match ('#include', line)):
                for i in range (len (superlist)):
                    superelement = re.split ('\.', superlist[i])
                    m = re.search (superelement[0], line)
                    if (m != None):
                        for j in range (len (newinclude)):
                            n = re.search (superelement[0], newinclude[j])
                            if (n != None):
                                old = re.split ('/', line)
                                new = re.split ('/', newinclude[j])
                                last_elem = len (old) - 1
                                outline = '#include <'
                                for k in range (len (new) - 1):
                                    outline += (new[k] + '/')
                                outline += old[last_elem]
                                fdout.write (outline)
                    else:
                        fdout.write (line)
            else:
                for i in range (len (superlist)):
                    superelement = re.split ('\.', superlist[i])
                    m = re.search (superelement[0], line)
                    if (m != None):
                        substitution = "siom_info::" + superelement[0]
                        newline = re.sub (superelement[0], substitution, line)
                        line = newline
                fdout.write (line)

        fdin.close ()
        fdout.close ()
        shutil.move (outfile, infile)


arg_list = sys.argv
info_file = arg_list[1]
info_include_dir = arg_list[2]

inst_path = os.getenv ('TDAQ_INST_PATH')
base_schema_dir  = inst_path + '/share/data/'
base_include_dir = inst_path + '/include/'

xmldoc = minidom.parse (info_file)
inclist = xmldoc.getElementsByTagName ('include')
classlist = xmldoc.getElementsByTagName ('class')

# Get the list of all direct superclasses for this file

superset = set ([])

for i in range (len (classlist)):
    super = classlist[i].getElementsByTagName ('superclass')

    for j in range (len (super)):
        keylist = super[j].attributes.keys ()
        vallist = super[j].attributes.values ()

        for k in range (len (keylist)):
            if keylist[k] == 'name':
                search_name = vallist[k].value + '.h'
                superset.add (search_name)

superlist = list (superset);

# Find the corresponding include files

filelist = []

os.path.walk (base_include_dir, find_file, None)

# Find the correct path for the include files

newinclude = []

for i in range (len (filelist)):
    m = re.split ("include/", filelist[i])
    n = len (m) - 1
    newinclude.append (m[n]);

# Scan generated include files

os.path.walk (info_include_dir, fix_file, None)
