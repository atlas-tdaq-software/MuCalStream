import os
import sys
import re
import shutil

def fix_file (d, f, packages):
    infile  = os.path.join (d, f)
    outfile = '/tmp/' + f + '_tmp'
    fdin = open (infile, 'r')
    fdout = open (outfile, 'w')

    ins = 0
    for line in fdin:
        if (ins == 1):
            for j in range (len (packages)):
                ins_line = "import " + packages[j] + ".*;\n"
                fdout.write (ins_line)
            fdout.write (line)
            ins = 0
        else:
            fdout.write (line)
            if (re.match ('package', line)):
                ins = 1

    fdin.close ()
    fdout.close ()
    shutil.move (outfile, infile)

# Main program

arg_list = sys.argv
info_java_dir = arg_list[1]

info_packages = []

for i in range (len (arg_list)):
    if (i > 1):
        info_packages.append (arg_list[i])

# Find the java files to be modified

filelist = os.listdir (info_java_dir)

for i in range (len (filelist)):
    fix_file (info_java_dir, filelist[i], info_packages)
