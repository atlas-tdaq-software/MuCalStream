#include <vector>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "mucaldal/MuCalStreamer.h"
#include "mucaldal/MuCalSelector.h"

#include "MuCalStream/pMuCalStreamer.h"
#include "MuCalStream/MuCalStreamerSelector.h"

void SIOM::MuCalStreamer::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					  const DalObject *appConfiguration,
					  const DalObject *objConfiguration,
					  bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const mucaldal::MuCalStreamer *conf =
    confDB->cast<mucaldal::MuCalStreamer> (objConfiguration);

  std::vector<const mucaldal::MuCalSelector *> s;
  std::vector<const mucaldal::MuCalSelector *>::const_iterator isel;

  s = conf->get_Selectors ();

  for (isel = s.begin (); isel != s.end (); ++isel)
  {
    SIOM::MuCalStreamerSelector selector ((*isel)->get_EtaMin (),
					  (*isel)->get_EtaMax (),
					  (*isel)->get_PhiMin (),
					  (*isel)->get_PhiMax ());
    m_selectors.push_back (selector);
  }

  if (debug)
  {
    std::vector<SIOM::MuCalStreamerSelector>::iterator i;

    for (i = m_selectors.begin (); i != m_selectors.end (); ++i)
      std::cout << "Selector configured for " 
		<< i->eta_min () << " < eta < " << i->eta_max ()
		<< i->phi_min () << " < eta < " << i->phi_max () << std::endl;
  }
}

void SIOM::MuCalStreamer::specific_unsetup ()
{
  m_selectors.clear ();
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createMuCalStreamer ();
}

SIOM::SIOMPlugin *createMuCalStreamer ()
{
  return (new SIOM::MuCalStreamer ());
}
