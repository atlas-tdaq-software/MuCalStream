#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <exception>
#include <vector>
#include <new>

#include "MuCalStream/MuCalFileHeader.h"

inline SIOM::MuCalFileHeader::MuCalFileHeader (unsigned int compressor_type,
					       unsigned int compression_level,
					       std::vector <SIOM::MuCalStreamerSelector> &selectors)
{
  uint32_t sel_size = selectors.size ();

  unsigned char *p;
  std::vector <SIOM::MuCalStreamerSelector>::iterator isel;

  uint32_t format_word = 0 | (MuCalFileFormatVersion & 0xffff) << 16 |
    (compressor_type & 0xff) << 8 | (compression_level & 0xff);

  _header_length = sizeof (MuCalFileHeaderMagic) + sizeof (format_word) +
    sizeof (_selector_structure) * sel_size;

  if ((_header_buffer = (unsigned char *) malloc (_header_length)) == (unsigned char *) NULL)
    throw (std::bad_alloc ());

  p = _header_buffer;
  memcpy (p, &MuCalFileHeaderMagic, sizeof (MuCalFileHeaderMagic));
  p += sizeof (MuCalFileHeaderMagic);
  memcpy (p, &format_word, sizeof (format_word));
  p += sizeof (format_word);
  memcpy (p, &sel_size, sizeof (sel_size));
  p += sizeof (sel_size);

  for (isel = selectors.begin (); isel != selectors.end (); ++isel)
  {
    _selector_structure.emin = isel->eta_min ();
    _selector_structure.emax = isel->eta_max ();
    _selector_structure.pmin = isel->phi_min ();
    _selector_structure.pmax = isel->phi_max ();

    memcpy (p, &_selector_structure, sizeof (_selector_structure));
    p += sizeof (_selector_structure);
  }
}
