#include <map>
#include <string>
#include <vector>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"
#include "ipc/partition.h"

// Package include files

#include "mucaldal/MuCalServerOutput.h"
#include "mucaldal/MuCalCompressor.h"

#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "MuCalStream/MuCalOutParameters.h"
#include "MuCalStream/MuCalDataSender.h"
#include "MuCalStream/pMuCalServerOutput.h"

void SIOM::MuCalServerOutput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					      const DalObject *appConfiguration,
					      const DalObject *objConfiguration,
					      bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const mucaldal::MuCalServerOutput *conf =
    confDB->cast<mucaldal::MuCalServerOutput> (objConfiguration);

  std::string activation_flag = conf->get_OutputActivationFlag ();
  std::string file_location = conf->get_FileLocation ();
  std::string file_tag = conf->get_FileTag ();
  std::string stream_type = conf->get_StreamType ();
  std::string stream_name = conf->get_StreamName ();
  int max_size = conf->get_MaxSize ();

  std::string  compressor_type;
  unsigned int compression_level = 0;

  const mucaldal::MuCalCompressor *compressor = conf->get_Compressor ();

  if (compressor != 0)
  {
    compressor_type   = compressor->get_CompressorType ();
    compression_level = compressor->get_CompressionLevel ();
  }

  const IPCPartition *partition = core_conf->get_partition ();

  if (debug)
  {
    std::cout << "Activation flag " << activation_flag << std::endl;
    std::cout << "File location " << file_location << std::endl;
    std::cout << "File tag " << file_tag << std::endl;
    std::cout << "Stream type " << stream_type << std::endl;
    std::cout << "Stream name " << stream_name << std::endl;
    std::cout << "Max size " << max_size << std::endl;
    std::cout << "Compressor type " << compressor_type << std::endl;
    std::cout << "Compression level " << compression_level << std::endl;
  }

  _addr = new SIOM::MuCalOutParameters (activation_flag,
					const_cast <IPCPartition *> (partition),
					file_location,
					file_tag,
					stream_type,
					stream_name,
					max_size,
					compressor_type,
					compression_level,
					core_conf);

  _dest.address (_addr);
}

void SIOM::MuCalServerOutput::specific_unsetup ()
{
  if (_addr)
  {
    delete _addr;
    _addr = 0;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createMuCalServerOutput ();
}

SIOM::SIOMPlugin *createMuCalServerOutput ()
{
  return (new SIOM::MuCalServerOutput ());
}
