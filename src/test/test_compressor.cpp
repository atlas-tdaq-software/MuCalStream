#include <string>

#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "ers/ers.h"
#include "DFThreads/DFThread.h"

#include "siom/core/SIOMCoreException.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/libBackTrace.h"

#include "MuCalStream/HandShaker.h"

#define ERR_ERROR      1
#define ERR_NO_ERROR   0

static bool stop = false;
static int first_run = 0;

SIOM::HandShaker *h = 0;

std::string program_name;
std::string host_name;

void test_compressor_sighandler (int sig)
{
  if (sig == SIGINT || sig == SIGQUIT || sig == SIGTERM)
  {
    printf ("Got signal %d, cleaning up...\n", sig);
    stop = true;
  }
  else if (sig == SIGSEGV || sig == SIGABRT || sig == SIGSYS)
  {
    printf ("Got signal %d, cleaning up...\n", sig);
    print_trace ();
    stop = true;
  }
}

void test_compressor_ExceptionHandler (std::exception &e)
{
  ENCAPSULATE_SIOM_EXCEPTION (e2, SIOM::SIOMCoreException,
			      UNCLASSIFIED, e, "Thread stopped");
  ers::error (e2);
  stop = true;
}

void test_compressor_set_handlers ()
{
  DFThread::set_exception_handler (test_compressor_ExceptionHandler);

  signal (SIGINT,  test_compressor_sighandler);
  signal (SIGQUIT, test_compressor_sighandler);
  signal (SIGTERM, test_compressor_sighandler);
  signal (SIGSEGV, test_compressor_sighandler);
  signal (SIGABRT, test_compressor_sighandler);
  signal (SIGSYS,  test_compressor_sighandler);
}

int test_compressor_init (char *progname)
{
  int err = ERR_NO_ERROR;
  char host[HOST_NAME_MAX];

  std::string dbAddress  = "oracle://atonr_conf/ATLAS_SFO_T0";
  std::string indexTable = "TEST_SEQINDEX";
  std::string runTable   = "TEST_RUN";
  std::string fileTable  = "TEST_FILE";
  std::string lumiTable  = "TEST_LUMI";

  test_compressor_set_handlers ();

  h = new SIOM::Compressor (dbAddress, indexTable, 
			    runTable, fileTable, lumiTable);

  program_name = progname;

  gethostname (host, HOST_NAME_MAX);
  host_name = host;

  return err;
}

void test_compressor_loop ()
{
  unsigned int iter = 0, niter = 3;

  unsigned int run          = first_run;
  unsigned int lumi_block   = 0;
  unsigned int file_size    = 1024 * 1024 * 1024;
  unsigned int event_number = 1024 * 1024;

  std::string stream_type   = "calibration";
  std::string stream        = "calibration_muon";
  std::string data_stream   = "data08_calib";
  std::string version_tag   = "o3";
  std::string guid          = "";
  std::string checksum      = "";

  std::string loc_file_name;
  std::string complete_file_name;

  while (!stop && iter < niter)
  {
    char srun[16];
    char sindex[16];

    unsigned int file_index   = 0;

    sprintf (srun, "%.7d", run);
    sprintf (sindex, "%.4d", file_index);

    loc_file_name = data_stream + "." + srun + "." +
                    stream + "_all.daq.RAW." + version_tag +
                    "._" + sindex + ".writing";

    complete_file_name = "/data0/data/" + loc_file_name;

    h->file_open (loc_file_name, file_index, program_name,
		  host_name, run, lumi_block,
		  stream_type, stream, data_stream,
		  guid, file_size, checksum,
		  event_number, complete_file_name, true);

    sleep (1);

    loc_file_name = data_stream + "." + srun + "." +
                    stream + "_all.daq.RAW." + version_tag +
                    "._" + sindex + ".data";

    complete_file_name = "/data0/data/" + loc_file_name;

    h->file_close (loc_file_name, file_index, program_name,
		   host_name, run, lumi_block,
		   stream_type, stream, data_stream,
		   guid, file_size, checksum,
		   event_number, complete_file_name, false);

    ++file_index;
    sprintf (sindex, "%.4d", file_index);

    loc_file_name = data_stream + "." + srun + "." +
                    stream + "_all.daq.RAW." + version_tag +
                    "._" + sindex + ".writing";

    complete_file_name = "/data0/data/" + loc_file_name;

    h->file_open (loc_file_name, file_index, program_name,
		  host_name, run, lumi_block,
		  stream_type, stream, data_stream,
		  guid, file_size, checksum,
		  event_number, complete_file_name, false);

    sleep (1);

    loc_file_name = data_stream + "." + srun + "." +
                    stream + "_all.daq.RAW." + version_tag +
                    "._" + sindex + ".data";

    complete_file_name = "/data0/data/" + loc_file_name;

    h->file_close (loc_file_name, file_index, program_name,
		   host_name, run, lumi_block,
		   stream_type, stream, data_stream,
		   guid, file_size, checksum,
		   event_number, complete_file_name, true);

    ++run; ++iter;
  }
}

void test_compressor_end ()
{
  if (h)
    delete h;
}

void test_compressor_opt (int argc, char **argv)
{
}

int main (int argc, char **argv)
{
  int err = ERR_NO_ERROR;

  if (argc > 1)
    first_run = atoi (argv[1]);

  if ((err = test_compressor_init (argv[0])) == ERR_NO_ERROR)
    test_compressor_loop ();

  test_compressor_end ();

  return err;
}
