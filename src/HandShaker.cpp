#include <string>

// SFOTZ include files

#include "SFOTZ/DBConnection.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/IndexTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/SFOTZIssues.h"

// SIOM include files

#include "siom/core/SIOMActiveObject.h"

#include "MuCalStream/HandShaker.h"
#include "MuCalStream/HandShakerCommander.h"

SIOM::HandShaker::~HandShaker ()
{
  if (_hcommander)
  {
    this->execute_all ();
    delete _hcommander;
  }

  this->join ();
}

SIOM::HandShaker::HandShaker (std::string &dbAddress,
			      std::string &indextable,
			      std::string &runtable,
			      std::string &filetable,
			      std::string &lumitable)
{
  _hcommander = new SIOM::HandShakerCommander (dbAddress,
					       indextable,
					       runtable,
					       filetable,
					       lumitable);

  this->start ();
}

void SIOM::HandShaker::execute_all ()
{
  while (_hcommander->test_command ())
  {
    try
    {
      _hcommander->execute_command ();
    }

    catch (SFOTZ::SFOTZIssue &i)
    {
      ers::warning (i);
    }
  }
}

void SIOM::HandShaker::run ()
{
  while (! _quit.load ())
  {
    this->execute_all ();
    sleep (1);
  }
}
