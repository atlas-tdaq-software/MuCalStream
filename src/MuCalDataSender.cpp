#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <zlib.h>

#include <string>
#include <vector>
#include <mutex>

// TDAQ include files

#include "is/infodictionary.h"
#include "rc/RunParams.h"

// siom include files

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

// Package include files

#include "mucal_info/muCalServerOutput.h"
#include "MuCalStream/MuCalDataSender.h"

SIOM::MuCalDataSender::MuCalDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
					SIOM::DataDestination *dest)
  : SIOM::DataSender (mpool, dest)
{
  auto par = reinterpret_cast <SIOM::MuCalOutParameters *> (dest->address ());

  _outpar = new SIOM::MuCalOutParameters (par->activation_flag (),
					  par->ipc_partition (),
					  par->file_location (),
					  par->file_tag (),
					  par->stream_type (),
					  par->stream_name (),
					  par->maxsize (),
					  par->compressor_type (),
					  par->compression_level (),
					  par->core_conf ());

  _index = 0;
  _written = 0;
  _event_number = 0;
  _descriptor = -1;
  _writeData = false;
  _firstInDataSet = true;

  char host[HOST_NAME_MAX];
  gethostname (host, HOST_NAME_MAX);
  _hostname = host;

  _application_name = "MUCalServer";

  std::string FileActivationFlag ("Always");

  if (_outpar->activation_flag () == FileActivationFlag)
    _writeData = true;

  // Start the sender thread

  this->start ();
}

SIOM::MuCalDataSender::~MuCalDataSender ()
{
  this->endOfRun ();
  this->join ();
  delete _outpar;
}

void SIOM::MuCalDataSender::startOfRun ()
{
  std::string ISActivationFlag ("IS");

  _index = 0;
  _firstInDataSet = true;

  if (_outpar->activation_flag () == ISActivationFlag)
  {
    // Get the flag from IS

    RunParams run_params;
    ISInfoDictionary is_dict (*(_outpar->ipc_partition ()));

    run_params.recording_enabled = 0;

    try
    {
      is_dict.findValue ("RunParams.RunParams", run_params);
    }

    catch (daq::is::Exception &ex)
    {
      std::cout << "Couldn't get RunParams from IS" << std::endl;
      //ers::error ("Couldn't get RunParams from IS");
    }

    _writeData = (run_params.recording_enabled != 0);
  }
}

void SIOM::MuCalDataSender::endOfRun ()
{
  // Wait a reasonable time to empty the buffers

  sleep (3);

  // Close the file and delete the handshaker

  if (_descriptor >= 0)
  {
    unsigned int lumi_block = 0;
    std::string project_tag = _outpar->file_tag ();
    std::string stream_type = _outpar->stream_type ();
    std::string stream = _outpar->stream_name ();

    _fileMutex.lock ();
    close (_descriptor);
    _descriptor = -1;
    _fileMutex.unlock ();

    _filename = this->publish_file (_filename, _local_filename);

    std::ostringstream oss;
    oss.width(8);
    oss.fill('0');
    oss << std::hex << std::uppercase << _checksum;
    std::string checksum = oss.str();

    poolCopy::Guid g (true);
    _guid = g.toString ();

    _handshaker->file_close (_local_filename, _index, _application_name,
			     _hostname, _RunNumber, lumi_block,
			     project_tag,
			     stream_type, stream, _dataset_name,
			     _guid, _written, checksum,
			     _event_number, _filename, true);

    delete _handshaker;
  }
}

int SIOM::MuCalDataSender::send (SIOM::MemoryBuffer <uint8_t> *b)
{
  size_t datalen;
  size_t len = datalen = b->n_objects () * sizeof (uint8_t);

  static unsigned int lumi_block = 0;

  if (_writeData)
  {
    int ret = 0;

    std::string dbAddress  = "oracle://atonr_conf/ATLAS_SFO_T0";
    std::string indexTable = "SFO_TZ_SEQINDEX";
    std::string runTable   = "SFO_TZ_RUN";
    std::string fileTable  = "SFO_TZ_FILE";
    std::string lumiTable  = "SFO_TZ_LUMIBLOCK";

    std::string project_tag = _outpar->file_tag ();
    std::string stream_type = _outpar->stream_type ();
    std::string stream = _outpar->stream_name ();

    if (_firstInDataSet)
    {
      RunParams rp;

      if (_outpar->core_conf ())
      {
	(_outpar->core_conf ())->get_runParams (rp);
	_RunNumber = rp.run_number;
      }
      else
	_RunNumber = 0;

      _handshaker = new HandShaker (dbAddress, indexTable, 
				    runTable, fileTable, lumiTable);

      _filename = this->build_filename (_outpar->file_location (),
					project_tag,
					stream_type,
					stream,
					_RunNumber, _index, _local_filename);

      _dataset_name = this->build_datasetname (project_tag,
					       stream_type,
					       stream,
					       _RunNumber);

      // Open output file.

      if ((_descriptor = open (_filename.c_str (),
			       O_WRONLY | O_CREAT | O_EXCL,
			       S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0)
      {
	std::cout << "Error in opening file " << _filename << std::endl;
      }

      std::ostringstream oss;
      oss.width(8);
      oss.fill('0');
      std::string checksum = oss.str();

      _written = 0;
      _event_number = 0;
      _checksum = adler32 (0L, Z_NULL, 0);

      poolCopy::Guid g (true);
      _guid = g.toString ();

      _handshaker->file_open (_local_filename, _index, _application_name,
			      _hostname, _RunNumber, lumi_block,
			      project_tag,
			      stream_type, stream, _dataset_name,
			      _guid, _written, checksum,
			      _event_number, _filename, _firstInDataSet);

      _firstInDataSet = false;
    }

    if (_written + datalen > _outpar->maxsize ())
    {
      close (_descriptor);
      _filename = this->publish_file (_filename, _local_filename);

      std::ostringstream oss;
      oss.width(8);
      oss.fill('0');
      oss << std::hex << std::uppercase << _checksum;
      std::string checksum = oss.str();

      _handshaker->file_close (_local_filename, _index, _application_name,
			       _hostname, _RunNumber, lumi_block,
			       project_tag,
			       stream_type, stream, _dataset_name,
			       _guid, _written, checksum,
			       _event_number, _filename, false);

      ++_index;
      _filename = this->build_filename (_outpar->file_location (),
					project_tag,
					stream_type, stream,
					_RunNumber, _index, _local_filename);

      if ((_descriptor = open (_filename.c_str (),
			       O_WRONLY | O_CREAT | O_EXCL,
			       S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0)
      {
	std::cout << "Error in opening file " << _filename << std::endl;
	return false;
      }

      _written = 0;
      _event_number = 0;
      _checksum = adler32 (0L, Z_NULL, 0);
      oss.fill('0');
      checksum = oss.str();

      poolCopy::Guid g (true);
      _guid = g.toString ();

      _handshaker->file_open (_local_filename, _index, _application_name,
			      _hostname, _RunNumber, lumi_block,
			      project_tag,
			      stream_type, stream, _dataset_name,
			      _guid, _written, checksum,
			      _event_number, _filename, _firstInDataSet);
    }

    // write data

    std::lock_guard <std::mutex> lock (_fileMutex);

    if (_descriptor >= 0) // Ignore spurious events between run
    {
      size_t page_size = b->page_size ();
      auto pages = b->get_pages ();

      for (auto page = pages.begin (); page != pages.end (); ++page)
      {
	unsigned int written = 0;
	size_t to_write = len > page_size ? page_size : len;

	if (to_write > 0)
	{
	  while (written < to_write)
	  {
	    if ((ret = write (_descriptor, ((char *) (*page) + written),
			      to_write - written)) < 0)
	    {
	      if (errno == EINTR)
		continue;
	      else
		break;
	    }
	    else
	      written += ret;
	  }

	  _checksum = adler32 (_checksum, (const Bytef *) (*page), written);
	  _written += written;
	  len -= to_write;
	}
      }

      if (ret <= 0)
	std::cout << "file write returned " << ret << std::endl;
      else
	++_event_number;
    }

    return (ret > 0 ? datalen : 0);
  }
  else
    return datalen;
}

// Ancillary methods

std::string SIOM::MuCalDataSender::build_datasetname (std::string &file_tag,
						      std::string &stream_type,
						      std::string &stream_name,
						      unsigned int run_number)
{
  char srun[16];
  sprintf (srun, "%.8d", run_number);

  std::string dataset_name = file_tag + "." + srun + "." +
                             stream_type + "_" + stream_name + ".daq.RAW";

  return dataset_name;
}

std::string SIOM::MuCalDataSender::build_filename (std::string &location,
						   std::string &file_tag,
						   std::string &stream_type,
						   std::string &stream_name,
						   unsigned int run_number,
						   unsigned int file_index,
						   std::string &local_file_name)
{
  char srun[16];
  char sindex[16];

  sprintf (srun, "%.8d", run_number);
  sprintf (sindex, "%.4d", file_index);

  local_file_name = file_tag + "." + srun + "." +
                    stream_type + "_" + stream_name + ".daq.RAW" +
                    "._" + sindex + ".writing";

  std::string filename = location + "/" + local_file_name;

  return filename;
}

std::string SIOM::MuCalDataSender::publish_file (std::string &filename,
						 std::string &local_filename)
{
  std::string newname = filename;
  newname.erase (newname.rfind (".writing", newname.length ()),
		 sizeof (".writing"));
  newname.append (".data");
  rename (filename.c_str (), newname.c_str ());

  filename = newname;

  newname = local_filename;
  newname.erase (newname.rfind (".writing", newname.length ()),
		 sizeof (".writing"));
  newname.append (".data");

  local_filename = newname;

  return filename;
}
