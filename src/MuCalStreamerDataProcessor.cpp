#include <string.h>

#include "ROSBufferManagement/Buffer.h"
#include "MuCalStream/MuCalStreamerDataProcessor.h"

unsigned int SIOM::MuCalStreamerDataProcessor::process_data (ROS::Buffer *input,
							     ROS::Buffer *output)
{
  unsigned int totlen = 0;

  int outPageSize = output->pageSize ();
  int freeSpace = outPageSize;

  ROS::Buffer::page_iterator pagein, pageout;

  for (pagein = input->begin (), pageout = output->begin ();
       pagein != input->end (); ++pagein)
  {
    int copied = 0;
    int size = (*pagein)->usedSize ();

    while (size > freeSpace)
    {
      if (freeSpace > 0)
      {
	memcpy ((char *) (*pageout)->address () + outPageSize - freeSpace,
		(char *) (*pagein)->address () + copied, freeSpace);

	size -= freeSpace;
	copied += freeSpace;
      }

      ++pageout;
      freeSpace = outPageSize;
    }

    if (size)
    {
      memcpy ((char *) (*pageout)->address () + outPageSize - freeSpace,
	      (char *) (*pagein)->address () + copied, size);

      freeSpace -= size;
      copied += size;
    }

    totlen += copied;
  }

  return totlen;
}
