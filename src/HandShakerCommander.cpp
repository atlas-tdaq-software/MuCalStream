#include <string>
#include <map>
#include <queue>
#include <mutex>

// SFOTZ include files

#include "SFOTZ/DBConnection.h"
#include "SFOTZ/RunTable.h"
#include "SFOTZ/IndexTable.h"
#include "SFOTZ/LumiBlockTable.h"
#include "SFOTZ/FileTable.h"
#include "SFOTZ/SFOTZIssues.h"

// SIOM include files

#include "MuCalStream/DBParameters.h"
#include "MuCalStream/HandShakerCommander.h"

SIOM::HandShakerCommander::HandShakerCommander (std::string &dbAddress,
						std::string &indextable,
						std::string &runtable,
						std::string &filetable,
						std::string &lumitable)
  : _dbAddress (dbAddress), _indexTableName (indextable),
    _runTableName (runtable), _fileTableName (filetable),
    _lumiTableName (lumitable)
{
  this->open_connection ();
  this->connect_tables ();
}

SIOM::HandShakerCommander::~HandShakerCommander ()
{
  this->disconnect_tables ();
  this->close_connection ();
}

void SIOM::HandShakerCommander::file_open (std::string &local_file_name,
					   unsigned int file_number,
					   std::string &application_name,
					   std::string &host_name,
					   unsigned int run_number,
					   unsigned int luminosity_block,
                                           std::string &project_tag,
					   std::string &stream_type,
					   std::string &stream,
					   std::string &data_stream_name,
					   std::string &guid,
					   unsigned int file_size,
					   std::string &checksum,
					   unsigned int event_number,
					   std::string &complete_file_name,
					   bool isfirst)
{
  SIOM::DBParameters par;

  SIOM::RunParameters *r = 0;
  SIOM::FileParameters *f = 0;
  SIOM::LumiBlockParameters *l = 0;

  if (isfirst)
  {
    r = new SIOM::RunParameters (application_name, run_number, stream_type,
				 stream, data_stream_name, project_tag);

    l = new SIOM::LumiBlockParameters (application_name, run_number,
				       luminosity_block, stream_type, stream);

    par.run_parameters = r;
    this->queue_command (SIOM::HandShakerCommander::RUNOPENED, par);
    par.lumi_parameters = l;
    this->queue_command (SIOM::HandShakerCommander::LBOPENED, par);
  }

  f = new SIOM::FileParameters (local_file_name, file_number, application_name,
				host_name, run_number, luminosity_block,
				stream_type, stream, guid, file_size,
				checksum, event_number, complete_file_name);

  par.file_parameters = f;
  this->queue_command (SIOM::HandShakerCommander::FILEOPENED, par);
}

void SIOM::HandShakerCommander::file_close (std::string &local_file_name,
					    unsigned int file_number,
					    std::string &application_name,
					    std::string &host_name,
					    unsigned int run_number,
					    unsigned int luminosity_block,
                                            std::string &project_tag,
					    std::string &stream_type,
					    std::string &stream,
					    std::string &data_stream_name,
					    std::string &guid,
					    unsigned int file_size,
					    std::string &checksum,
					    unsigned int event_number,
					    std::string &complete_file_name,
					    bool islast)
{
  SIOM::DBParameters par;

  SIOM::RunParameters *r = 0;
  SIOM::FileParameters *f = 0;
  SIOM::LumiBlockParameters *l = 0;

  f = new SIOM::FileParameters (local_file_name, file_number, application_name,
				host_name, run_number, luminosity_block,
				stream_type, stream, guid, file_size,
				checksum, event_number, complete_file_name);

  par.file_parameters = f;
  this->queue_command (SIOM::HandShakerCommander::FILECLOSED, par);

  if (islast)
  {
    r = new SIOM::RunParameters (application_name, run_number, stream_type,
				 stream, data_stream_name, project_tag);

    l = new SIOM::LumiBlockParameters (application_name, run_number,
				       luminosity_block, stream_type, stream);

    par.run_parameters = r;
    this->queue_command (SIOM::HandShakerCommander::RUNCLOSED, par);
    par.lumi_parameters = l;
    this->queue_command (SIOM::HandShakerCommander::LBCLOSED, par);
  }
}

void SIOM::HandShakerCommander::execute_command ()
{
  std::lock_guard <std::mutex> lock (_q_mutex);
  std::pair <SIOM::HandShakerCommander::CommandType, SIOM::DBParameters> q_elem = _command_queue.front ();
  _command_queue.pop ();

  SIOM::DBParameters par = q_elem.second;

  try
  {
    switch (q_elem.first)
    {
      case SIOM::HandShakerCommander::RUNOPENED:
	this->add_run (par.run_parameters);
	delete par.run_parameters;
	break;
      case SIOM::HandShakerCommander::RUNCLOSED:
	this->close_run (par.run_parameters);
	delete par.run_parameters;
	break;
      case SIOM::HandShakerCommander::LBOPENED:
	this->add_lumi (par.lumi_parameters);
	delete par.lumi_parameters;
	break;
      case SIOM::HandShakerCommander::LBCLOSED:
	this->close_lumi (par.lumi_parameters);
	delete par.lumi_parameters;
	break;
      case SIOM::HandShakerCommander::FILEOPENED:
	this->add_file (par.file_parameters);
	delete par.file_parameters;
	break;
      case SIOM::HandShakerCommander::FILECLOSED:
	this->close_file (par.file_parameters);
	delete par.file_parameters;
	break;
      default:
	break;
    }
  }

  catch (...)
  {
    switch (q_elem.first)
    {
      case SIOM::HandShakerCommander::RUNOPENED:
	delete par.run_parameters;
	break;
      case SIOM::HandShakerCommander::RUNCLOSED:
	delete par.run_parameters;
	break;
      case SIOM::HandShakerCommander::LBOPENED:
	delete par.lumi_parameters;
	break;
      case SIOM::HandShakerCommander::LBCLOSED:
	delete par.lumi_parameters;
	break;
      case SIOM::HandShakerCommander::FILEOPENED:
	delete par.file_parameters;
	break;
      case SIOM::HandShakerCommander::FILECLOSED:
	delete par.file_parameters;
	break;
    }

    throw;
  }
}
